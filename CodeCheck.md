# 🔍 Gradio YOLOv5 Det 代码检查

## 创建人：曾逸夫

### 🛡️ Gitee CodeCheck

|  版本  | 代码问题数 | 代码平均圈复杂度 | 代码重复率 | 有效代码行数 | 发行时间 |
| :--: | :---: | :------: | :---: | :----: | :--: |
| v0.1 |       |          |       |        |      |

用于衡量代码复杂度（与代码可维护、测试性相关），圈复杂度越高，重构码出错的概率越高。具体风险评估建议如下：<span style="color:green;">1-10（低风险）</span>；<span style="color:#EEAD0E;">11-20（中风险）</span>；<span style="color:red;">21-50（高风险）</span>；<span style="color:#8B2323;">51+（极高风险）</span>

### 🛡️ [lizard](https://github.com/terryyin/lizard) 代码检查

|  版本  | Total nloc | Avg.NLOC | AvgCCN | Avg.token | Fun Cnt |
| :--: | :--------: | :------: | :----: | :-------: | :-----: |
| v0.1 |            |          |        |           |         |



### 💡 参考

- Gitee CodeCheck
- [lizard](https://github.com/terryyin/lizard)
